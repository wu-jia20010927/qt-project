# Qt-project

#### 介绍
利用Qt制作了一个翻转金币的小游戏，点击任意一个硬币，其周围硬币也会跟着翻转，玩家需要把所有银币翻转为金币，即可取得胜利

#### 使用说明

1.  解压CoinFlip-ending.zip文件，在Qt下运行ConFlip.pro文件
2.  鼠标点击start按钮，选择关卡
3.  鼠标点击硬币开始闯关
